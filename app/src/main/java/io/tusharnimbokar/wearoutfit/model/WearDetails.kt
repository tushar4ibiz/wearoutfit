package io.tusharnimbokar.wearoutfit.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class WearDetails(
    @ColumnInfo var title: String = "",
    @ColumnInfo var type: String = "",
    @ColumnInfo(name = "isFavourite")var isFavourite :Boolean= false,
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB) var image: ByteArray? = null
) {
    @PrimaryKey(autoGenerate = true)
    var uid = 0
}