package io.tusharnimbokar.wearoutfit.common

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import io.tusharnimbokar.wearoutfit.model.WearDetails
import io.tusharnimbokar.wearoutfit.ui.fragment.ViewPagerFragment.Companion.newInstance

class ViewPagerAdapter(
    fragmentActivity: FragmentActivity
) :
    FragmentStateAdapter(fragmentActivity) {
    var wearList = java.util.ArrayList<WearDetails>()

    override fun createFragment(position: Int): Fragment {
        return newInstance(position, wearList.get(position))
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }

    fun setList(wear :List<WearDetails>){
        wearList.clear()
        wearList.addAll(wear)
    }


    override fun getItemCount(): Int {
        return wearList?.size!!
    }

    companion object {
        // private const val data.
    }
}