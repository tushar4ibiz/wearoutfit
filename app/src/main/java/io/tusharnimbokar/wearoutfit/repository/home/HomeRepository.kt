package io.tusharnimbokar.wearoutfit.repository.home

import io.tusharnimbokar.wearoutfit.Utils.AppConfig
import io.tusharnimbokar.wearoutfit.model.WearDetails
import io.tusharnimbokar.wearoutfit.repository.db.DbDao

class HomeRepository(private val dbDao: DbDao) {


    val getTopWear = dbDao.getAllWear(wearType = AppConfig().TYPE_TOP)
    val getBottomWear = dbDao.getAllWear(AppConfig().TYPE_BOTTOM)

    suspend fun insert(wear: WearDetails): Long? {
        return dbDao?.insertWear(wear)
    }

    fun markFavourire(id: Int, isFav: Boolean?) = dbDao.markFavourite(id = id, isFav = isFav)

    fun getLastWearUID(type: String) = dbDao?.last(type)

    fun getFirstWearUID(type: String) = dbDao?.first(type)

}
