package io.tusharnimbokar.wearoutfit.repository.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.tusharnimbokar.wearoutfit.model.WearDetails

@Dao
interface DbDao {

    @Query("SELECT * FROM weardetails WHERE type IN (:wearType)")
     fun getAllWear(wearType: String): LiveData<List<WearDetails>>

    @Query("SELECT uid FROM weardetails WHERE type IN (:wearType) ORDER BY UID DESC LIMIT 0,1")
     fun last(wearType: String): Int

    @Query("SELECT uid FROM weardetails WHERE type IN (:wearType) ORDER BY UID ASC LIMIT 0,1")
     fun first(wearType: String): Int

    @Query("UPDATE weardetails SET isFavourite = :isFav WHERE UID = :id")
    fun markFavourite(id: Int, isFav: Boolean?): Int


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertWear(wear: WearDetails): Long
}