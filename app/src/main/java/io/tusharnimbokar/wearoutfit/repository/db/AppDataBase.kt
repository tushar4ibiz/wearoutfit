package io.tusharnimbokar.wearoutfit.repository.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import io.tusharnimbokar.wearoutfit.model.WearDetails

@Database(entities = [WearDetails::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun dbDao(): DbDao

    companion object{
        @Volatile
        private var INSTANCE : AppDatabase? = null
        fun getInstance(context: Context):AppDatabase{
            synchronized(this){
                var instance = INSTANCE
                if(instance==null){
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        "outfit_database"
                    ).build()
                }
                return instance
            }
        }

    }
}
