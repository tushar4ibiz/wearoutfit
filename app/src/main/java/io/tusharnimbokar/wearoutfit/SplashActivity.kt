package io.tusharnimbokar.wearoutfit

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import io.tusharnimbokar.wearoutfit.ui.HomeActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.delay

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        supportActionBar?.hide()

        CoroutineScope(Dispatchers.Main).async {
            delay(3000)
            launchActivity()
        }

    }

    fun launchActivity() {
        startActivity(
            Intent(this, HomeActivity::class.java)
        )
        finish()

    }
}