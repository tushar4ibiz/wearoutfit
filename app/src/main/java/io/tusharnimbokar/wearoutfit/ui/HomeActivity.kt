package io.tusharnimbokar.wearoutfit.ui

import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.view.View
import android.view.Window
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.varunest.sparkbutton.SparkEventListener
import io.tusharnimbokar.waeroutfit.viewmodel.ViewModelFactory
import io.tusharnimbokar.wearoutfit.R
import io.tusharnimbokar.wearoutfit.Utils.AppConfig
import io.tusharnimbokar.wearoutfit.Utils.Utility
import io.tusharnimbokar.wearoutfit.common.BitmapManager
import io.tusharnimbokar.wearoutfit.common.ViewPagerAdapter
import io.tusharnimbokar.wearoutfit.databinding.ActivityHomeBinding
import io.tusharnimbokar.wearoutfit.model.WearDetails
import io.tusharnimbokar.wearoutfit.repository.db.AppDatabase
import io.tusharnimbokar.wearoutfit.repository.home.HomeRepository
import io.tusharnimbokar.wearoutfit.viewmodel.home.HomeViewModel
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.select_storage_option_dialog.*
import java.io.IOException
import java.util.*

class HomeActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var binding: ActivityHomeBinding

    //Camera permission
    private val REQUEST_CAMERA = 0

    //File permission
    private var SELECT_FILE: Int = 1

    var isTopImage = false
    var bitmap: Bitmap? = null
    var imageTitle = "image"

    //ExitApp
    private var exitApp = false


    var topWearList = ArrayList<WearDetails>()
    var bottomWearList = ArrayList<WearDetails>()
    lateinit var topWearAdapter: ViewPagerAdapter
    lateinit var bottomWearAdapter: ViewPagerAdapter

    private lateinit var viewiModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)

        //Binding all click listeners
        binding.addTop.setOnClickListener(this)
        binding.addBottom.setOnClickListener(this)
        binding.imgShuffle.setOnClickListener(this)

        val dao = AppDatabase.getInstance(application).dbDao()
        val repository = HomeRepository(dao)
        val factory = ViewModelFactory(repository)

        viewiModel = ViewModelProvider(this, factory).get(HomeViewModel::class.java)
        binding.lifecycleOwner = this

        viewiModel.message.observe(this, {
            it.getContentIfNotHandled()?.let {
                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            }
        })



        binding.sparkButton.setEventListener(object : SparkEventListener {
            override fun onEvent(button: ImageView, buttonState: Boolean) {
                if (buttonState) {
                    binding.sparkButton.isClickable = false
                    binding.sparkButton.isEnabled = false
                    if (topWearList.size > 0 && bottomWearList.size > 0) {
                        markFavroute()
                        binding.sparkButton.setActiveImage(R.drawable.ic_heart_pink)
                    } else {
                        binding.sparkButton.setActiveImage(R.drawable.ic_heart_gray)
                        Toast.makeText(
                            applicationContext,
                            "top and bottom wear not should be empty",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    binding.sparkButton.isClickable = false
                    binding.sparkButton.isChecked = false
                    binding.sparkButton.setActiveImage(R.drawable.ic_heart_gray)


                }
            }

            override fun onEventAnimationEnd(button: ImageView, buttonState: Boolean) {
            }

            override fun onEventAnimationStart(button: ImageView, buttonState: Boolean) {
            }
        })


        binding.viewPagerTop.registerOnPageChangeCallback(object :
            ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if (topWearList.size > 0 && bottomWearList.size > 0) {
                    checkFavourite(
                        topWearList[position].isFavourite,
                        bottomWearList[binding.viewPagerBottom.currentItem].isFavourite
                    )
                }
            }

        })

        binding.viewPagerBottom.registerOnPageChangeCallback(object :
            ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if (topWearList.size > 0 && bottomWearList.size > 0) {
                    checkFavourite(
                        topWearList[binding.viewPagerTop.currentItem].isFavourite,
                        bottomWearList[position].isFavourite
                    )
                }
            }
        })

        initViewPagerAdapter()
    }

    private fun initViewPagerAdapter() {
        topWearAdapter = ViewPagerAdapter(this)
        bottomWearAdapter = ViewPagerAdapter(this)
        binding.viewPagerTop.adapter = topWearAdapter
        binding.viewPagerBottom.adapter = bottomWearAdapter
        loadList()
    }


    private fun loadList() {
        viewiModel.getAllBottomWear.observe(this, {
            bottomWearAdapter.setList(it)
            bottomWearAdapter.notifyDataSetChanged()
            bottomWearList = it as ArrayList<WearDetails>

        })

        viewiModel.getAllTopWear.observe(this, {
            topWearAdapter.setList(it)
            topWearAdapter.notifyDataSetChanged()
            topWearList = it as ArrayList<WearDetails>

        })

    }

    private fun checkFavourite(isFavTop: Boolean, isFavBottom: Boolean) {

        if (isFavTop && isFavBottom) {
            binding.sparkButton.isChecked = true
            binding.sparkButton.setActiveImage(R.drawable.ic_heart_pink)


        } else {
            binding.sparkButton.isChecked = false
            binding.sparkButton.setActiveImage(R.drawable.ic_heart_gray)
        }
    }

    private fun markFavroute() {
        viewiModel.markFavourite(topWearList[binding.viewPagerTop.currentItem].uid, true)
        viewiModel.markFavourite(bottomWearList[binding.viewPagerBottom.currentItem].uid, true)
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.addTop -> {
                isTopImage = true
                if (Utility.checkPermission(this@HomeActivity)) {
                    showUploadChooser()
                }
            }
            R.id.addBottom -> {
                isTopImage = false
                if (Utility.checkPermission(this@HomeActivity)) {
                    showUploadChooser()
                }
            }
            R.id.imgShuffle -> {
                binding.imgShuffle.setColorFilter(ContextCompat.getColor(this, R.color.colorAccent))
                imgShuffle.startAnimation(
                    AnimationUtils.loadAnimation(
                        applicationContext,
                        R.anim.rotate
                    )
                )
                loadRandom()
            }
        }

    }

    private fun loadRandom() {
        binding.viewPagerTop.currentItem =
            (0..topWearList.size).random().toString().toInt()
        binding.viewPagerBottom.currentItem =
            (0..bottomWearList.size).random().toString().toInt()
        binding.viewPagerTop.adapter?.notifyDataSetChanged()
        binding.viewPagerBottom.adapter?.notifyDataSetChanged()
    }

    fun showUploadChooser() {
        val result: Boolean = Utility.checkPermission(this@HomeActivity)
        val dialog = Dialog(this@HomeActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.select_storage_option_dialog)
        dialog.window?.attributes?.windowAnimations = R.style.dialogAnimation
        if (isTopImage) {
            dialog.tvHeading.text = resources.getString(R.string.top_outfit)
        } else {
            dialog.tvHeading.text = resources.getString(R.string.bottom_outfit)
        }
        dialog.findViewById<View>(R.id.tvCamera)
            .setOnClickListener { v: View? ->

                if (result) {
                    cameraIntent()
                }
                dialog.dismiss()
            }
        dialog.findViewById<View>(R.id.tvGalary)
            .setOnClickListener { v: View? ->
                if (result) {
                    galleryIntent()
                }
                dialog.dismiss()
            }

        dialog.findViewById<View>(R.id.tvCancel)
            .setOnClickListener { v: View? ->
                dialog.dismiss()
            }
        val metrics = resources.displayMetrics
        val width = metrics.widthPixels
        dialog.window?.setLayout(6 * width / 8, LinearLayout.LayoutParams.WRAP_CONTENT)
        dialog.show()
    }


    private fun galleryIntent() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT //
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE)
    }

    private fun cameraIntent() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, REQUEST_CAMERA)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_FILE) onSelectFromGalleryResult(data)
            else if (requestCode == REQUEST_CAMERA) {
                data?.let { onCaptureImageResult(it) }
            }
        }
    }

    private fun onCaptureImageResult(data: Intent) {
        bitmap = data.extras!!["data"] as Bitmap?
        var lastUID = 0
        try {
            lastUID = if (isTopImage) {
                viewiModel.getFirstUID(AppConfig().TYPE_TOP)
            } else {
                viewiModel.getFirstUID(AppConfig().TYPE_BOTTOM)
            }

        } catch (e: Exception) {
        }
        imageTitle = "Image" + (lastUID + 1).toString()
        insertImages()
    }

    private fun onSelectFromGalleryResult(data: Intent?) {
        if (data != null) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(
                    applicationContext.contentResolver,
                    data.data
                )

                var lastUID = 0
                try {
                    lastUID = if (isTopImage) {
                        viewiModel.getLastUID(AppConfig().TYPE_TOP)
                    } else {
                        viewiModel.getLastUID(AppConfig().TYPE_BOTTOM)
                    }

                } catch (e: Exception) {
                }
                imageTitle = AppConfig().IMAGE_NAME + (lastUID + 1).toString()
            } catch (e: IOException) {
                e.printStackTrace()
            }

            insertImages()
        }
    }


    private fun insertImages() {
        val image: ByteArray? = bitmap?.let { BitmapManager().bitmapToByte(it) }
        if (isTopImage) {
            viewiModel.insertWear(
                AppConfig().TYPE_TOP, WearDetails(
                    imageTitle,

                    AppConfig().TYPE_TOP,
                    false,
                    image
                )
            )
        } else {
            viewiModel.insertWear(
                AppConfig().TYPE_BOTTOM, WearDetails(
                    imageTitle, AppConfig().TYPE_BOTTOM, false, image
                )
            )
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            Utility.REQUEST_ID_MULTIPLE_PERMISSIONS -> if (ContextCompat.checkSelfPermission(
                    this@HomeActivity,
                    android.Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                deniedPermissionDialog()

            } else if (ContextCompat.checkSelfPermission(
                    this@HomeActivity,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                deniedPermissionDialog()

            } else {
                showUploadChooser()
            }
        }
    }

    private fun deniedPermissionDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.permission_title)
        builder.setPositiveButton(android.R.string.ok, null)
        builder.setNegativeButton(
            R.string.cancel
        ) { dialog, _ ->
            dialog.dismiss()
            finishAffinity()
        }
        builder.setMessage(R.string.permission_message)
        builder.setOnDismissListener(DialogInterface.OnDismissListener {
            if (Utility.checkPermission(this@HomeActivity)) {
                showUploadChooser()
            }
        })
        builder.show()
    }

    override fun onBackPressed() {
        if (exitApp) {
            Intent.FLAG_ACTIVITY_CLEAR_TOP
            Intent.FLAG_ACTIVITY_CLEAR_TASK
            this@HomeActivity.finish()
            finishAffinity()
        } else {
            Toast.makeText(
                this, "Press Back again to Exit.",
                Toast.LENGTH_SHORT
            ).show()
            exitApp = true
            Handler().postDelayed(Runnable { exitApp = false }, 3 * 1000)
        }
    }

}



