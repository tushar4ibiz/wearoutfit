package io.tusharnimbokar.wearoutfit.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import io.tusharnimbokar.wearoutfit.R
import io.tusharnimbokar.wearoutfit.common.BitmapManager
import io.tusharnimbokar.wearoutfit.databinding.FragmentBottomBinding
import io.tusharnimbokar.wearoutfit.model.WearDetails

class ViewPagerFragment : Fragment() {
    lateinit var binding: FragmentBottomBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bottom, container, false)
        binding.imgBottom.setImageBitmap(image!!.image?.let { BitmapManager().byteToBitmap(it) })

        binding.bottomImage = image

        return binding.root
    }

    companion object {
        private const val ARG_COUNT = "param1"
        var image: WearDetails? = null

        @JvmStatic
        fun newInstance(
            counter: Int?,
            wearDetails: WearDetails?
        ): ViewPagerFragment {
            val fragment = ViewPagerFragment()
            val args = Bundle()
            args.putInt(ARG_COUNT, counter!!)
            image = wearDetails

            fragment.arguments = args
            return fragment
        }
    }
}