package io.tusharnimbokar.wearoutfit.viewmodel.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.tusharnimbokar.wearoutfit.common.Event
import io.tusharnimbokar.wearoutfit.model.WearDetails
import io.tusharnimbokar.wearoutfit.repository.home.HomeRepository
import kotlinx.coroutines.*


class HomeViewModel(val homeRepository: HomeRepository) : ViewModel() {

    val getAllTopWear = homeRepository.getTopWear
    val getAllBottomWear = homeRepository.getBottomWear
    private var isTopWear = false
    private lateinit var wear : WearDetails


    private val statusMessage = MutableLiveData<Event<String>>()

    val message : LiveData<Event<String>>
        get() = statusMessage


    /**
     * Insert a wear
     * @param wearDetails
     */
    fun insertWear(type:String,wearDetails: WearDetails)= viewModelScope.launch {
        val newRowId = homeRepository.insert(wearDetails)
        if (newRowId != null) {
            if(newRowId>-1) {
                statusMessage.value = Event("New $type wear added")
            }else{
                statusMessage.value = Event("Error Occurred")
            }
        }
    }

    /**
     * Mark a combination favourite
     * @param id //Creating a new combination id
     */
    fun markFavourite(id: Int, isFav: Boolean?){
        var newRowId = 0

        runBlocking(CoroutineScope(Dispatchers.IO).coroutineContext) {
            newRowId = withContext(Dispatchers.IO) { homeRepository.markFavourire(id,isFav) }
        }
        if (newRowId > -1) {
            statusMessage.value = Event("mark favourite  $newRowId")
        } else {
            statusMessage.value = Event("Error Occurred")
        }
    }

    //Get last id of wear
    fun getLastUID(type: String): Int {
        var uid: Int = 0
        runBlocking(CoroutineScope(Dispatchers.IO).coroutineContext) {
            uid = withContext(Dispatchers.IO) { homeRepository.getLastWearUID(type) }
        }
        return uid
    }

    //Get first id of wear
    fun getFirstUID(type: String): Int {
        var uid: Int = 0
        runBlocking(CoroutineScope(Dispatchers.IO).coroutineContext) {
            uid = withContext(Dispatchers.IO) { homeRepository.getFirstWearUID(type) }
        }
        return uid
    }
}